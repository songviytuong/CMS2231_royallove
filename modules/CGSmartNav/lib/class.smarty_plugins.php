<?php
namespace CGSmartNav;

final class smarty_plugins
{
    protected static $itemcount = 0;
    protected function __construct() {}

    public static function init()
    {
        $smarty = \Smarty_CMS::get_instance();
        $smarty->register_function('nav_create','\CGSmartNav\smarty_plugins::nav_create');
        $smarty->register_function('nav_add_content','\CGSmartNav\smarty_plugins::nav_add_content');
        $smarty->register_function('nav_add_item','\CGSmartNav\smarty_plugins::nav_add_item');
        $smarty->register_function('nav_remove_item','\CGSmartNav\smarty_plugins::nav_remove_item');
        $smarty->register_function('nav_set_active','\CGSmartNav\smarty_plugins::nav_set_active');
        $smarty->register_function('nav_render','\CGSmartNav\smarty_plugins::nav_render');
    }

    // {nav_create name='foo'}
    public static function nav_create($params,&$tpl)
    {
        $name = \cge_param::get_string($params,'nav');
        $name = \cge_param::get_string($params,'name',$name);
        if( !$name ) return;

        if( $name ) {
            $maxdepth = \cge_param::get_int($params,'maxdepth',1000);
            $maxdepth = max(-1,$maxdepth);
            SmartNav::create($name,$maxdepth);
        }
    }

    public static function nav_add_content($params,&$tpl)
    {
        $walk = function(\cms_content_tree &$node,&$parent_nav,$maxdepth,$force,$depth = 0) use (&$walk) {
            // add this node
            if( !$node->get_tag('alias') ) return;
            $content_obj = $node->getContent();
            if( !$content_obj ) return;
            if( !$content_obj->Active() ) return;
            if( !$force && !$content_obj->ShowInMenu() ) return;
            if( !$content_obj->IsPermitted() ) return;

            $item = new SmartNavItem();
            switch( $content_obj->Type() ) {
            case 'separator':
                $item->type = $item::SEPARATOR;
                break;
            case 'sectionheader':
                $item->type = $item::SECTIONHEADER;
                break;
            default:
                $item->type = $item::ITEM;
                break;
            }
            $item->type = $content_obj->Type();
            $item->url = $content_obj->GetURL();
            $item->text = $content_obj->Name();
            $item->name = $content_obj->Alias();
            $item->title = $content_obj->TitleAttribute();
            if( ! $item->is_valid() ) throw new \LogicException('Problem building a valid SmartNavItem from content object '.$content_obj->Id());

            if( $depth < $maxdepth ) {
                $children = $node->get_children();
                for( $i = 0; $i < count($children); $i++ ) {
                    $child = $children[$i];
                    $walk($children[$i],$item,$maxdepth,$force,$depth+1);
                }
            }
            $parent_nav->add_child($item);
        };

        $name = \cge_param::get_string($params,'nav');
        if( !$name ) return;
        $start = \cge_param::get_string($params,'start');
        $childrenof = \cge_param::get_string($params,'childrenof');
        $maxdepth = \cge_param::get_int($params,'maxdepth',1000);
        $maxdepth = max(-1,$maxdepth);
        if( $maxdepth == 0 ) $maxdepth = -1;

        $nav = SmartNav::get($name);
        if( !$nav ) $nav = SmartNav::create($name);

        $parent_name = \cge_param::get_string($params,'parent');
        if( $parent_name ) {
            $nav = $nav->find($parent_name);
            if( !$nav ) return;
        }

        // now find the start element.
        $force = \cge_param::get_bool($params,'showall');
        $node = \CmsApp::get_instance()->GetHierarchyManager();
        if( $start ) {
            $node = $node->find_by_tag('alias',$start);
        }
        else if( $childrenof ) {
            $tmp = $node->find_by_tag('alias',$childrenof);
            if( !$tmp ) return;
            $children = $tmp->get_children();
            if( count($children) ) {
                foreach( $children as $child ) {
                    $walk($child,$nav,$maxdepth,$force);
                }
            }
        }
        if( !$node ) return;

        // now copy (up to maxdepth) all of the nodes from $node downwards
        $walk($node,$nav,$maxdepth,$force);
    }

    public static function nav_add_item($params,&$tpl)
    {
        $navname = \cge_param::get_string($params,'nav');
        if( !$navname ) return;

        $type = \cge_param::get_string($params,'type');
        $name = \cge_param::get_string($params,'name');
        $url = \cge_param::get_string($params,'url');
        $text = \cge_param::get_string($params,'text');
        $title = \cge_param::get_string($params,'title');
        $class = \cge_param::get_string($params,'class');
        $target = \cge_param::get_string($params,'target');
        $id = \cge_param::get_string($params,'id');
        $key = \cge_param::get_string($params,'key');
        $parent_name =  \cge_param::get_string($params,'parent');

        $nav = SmartNav::get($navname);
        if( !$nav ) return;

        if( $parent_name ) {
            $nav = $nav->find($parent_name);
            if( !$nav ) return;
        }

        if( !$name ) {
            // name is now optional.
            $n = self::$itemcount+1;
            $name = 'item_'.$n;
        }

        switch( $type ) {
        case 'separator':
            $type = SmartNavItem::SEPARATOR;
            break;
        case 'header':
        case 'sectionheader':
            $type = SmartNavItem::SECTIONHEADER;
            break;
        default:
            $type = SmartNavItem::ITEM;
            break;
        }

        $item = new SmartNavItem();
        $item->type = $type;
        $item->name = $name;
        $item->url = $url;
        $item->text = $text;
        $item->title = $title;
        $item->target = $target;
        $item->class = $class;
        $item->id = $id;
        $item->key = $key;
        $nav->add_child($item);
        self::$itemcount++;
    }

    public static function nav_remove_item($params,&$tpl)
    {
        $name = \cge_param::get_string($params,'nav');
        if( !$name ) return;

        $nav = SmartNavigation::get($name);
        if( !$nav ) return;

        $item = \cge_utils::get_param($params,'item');
        $url = \cge_param::get_string($params,'url');
        if( $item ) $url = $item->url;
        $nav->remove_child($url);
    }

    public static function nav_set_active($params,&$tpl)
    {
        $navname = \cge_param::get_string($params,'nav');
        if( !$navname ) return;
        $name = \cge_param::get_string($params,'item');
        if( !$name ) return;

        \cge_tmpdata::set('CGSmartNav::active::'.$navname, $name);
    }

    public static function nav_render($params,&$tpl)
    {
        $navname = \cge_param::get_string($params,'nav');
        if( !$navname ) return;
        $template = \cge_param::get_string( $params, 'template' );

        $parms = [ 'nav'=>$navname, 'template'=>$template ];
        $mod = \cms_utils::get_module('CGSmartNav');
        return $mod->DoActionBase('default', 'cntnt01', $parms, \cms_utils::get_current_pageid(), $tpl );
    }
} // class
