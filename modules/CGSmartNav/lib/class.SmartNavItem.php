<?php

namespace CGSmartNav;

class SmartNavItem extends \cms_tree
{
    const ITEM = '_item';
    const SEPARATOR = '_separator';
    const SECTIONHEADER = '_sectionheader';

    public function __construct($params = null)
    {
        if( !is_array($params) ) return;
        $url = \cge_param::get_string($params,'url');
        $text = \cge_param::get_string($params,'text');
        $name = \cge_param::get_string($params,'name');
        $type = \cge_param::get_string($params,'type');

        if( !$type ) $type = self::ITEM;
        parent::__construct($params);
    }

    public function __get($key)
    {
        switch( $key ) {
        case 'type':
        case 'url':
        case 'text':
        case 'title':
        case 'class':
        case 'key':
        case 'id':
        case 'name':
        case 'children':
        case 'active':
        case 'active_parent':
        case 'target':
            return $this->get_tag($key);

        default:
            throw new \LogicException("$key is not a gettable member of ".__CLASS__);
        }
    }

    public function __set($key,$val)
    {
        switch( $key ) {
        case 'type':
        case 'url':
        case 'text':
        case 'title':
        case 'class':
        case 'key':
        case 'id':
        case 'name':
        case 'active':
        case 'active_parent':
        case 'target':
            $this->set_tag($key,$val);
            break;

        default:
            throw new \LogicException("$key is not a settable member of ".__CLASS__);
        }
    }

    public function is_valid()
    {
        if( !$this->type ) $this->type = self::ITEM;
        if( !$this->name ) return FALSE;
        switch( $this->type ) {
        case self::ITEM:
            // url and text required
            if( !$this->url || !$this->text ) return FALSE;
            break;
        case self::SEPARATOR:
            // nothing required
            break;
        case self::SECTIONHEADER:
            // text required
            if( !$this->text ) return FALSE;
            break;
        }

        return TRUE;
    }

    public function add_child(SmartNavItem $item)
    {
        if( !$item->is_valid() ) {
            throw new \LogicException('Attempt to add invalid nav item');
        }
        $this->add_node($item);
    }

    public function &find($parent_name)
    {
        return $this->find_by_tag('name',$parent_name);
    }

    public function remove_child($name)
    {
        $node = $this->find_by_tag('name',$name);
        if( $node ) $this->remove_node($node);
    }
}