<?php

namespace CGSmartNav;

class SmartNav
{
    private static $_navs;

    protected function __construct() {}

    public static function &create($name)
    {
        $name = trim($name);
        if( !$name ) throw new \LogicException('Invalid name specified to '.__METHOD__);

        if( !isset(self::$_navs[$name]) ) self::$_navs[$name] = new SmartNavItem(array('url'=>'#','name'=>'__root','text'=>'-'));
        return self::$_navs[$name];
    }

    public static function &get($name)
    {
        $name = trim($name);
        if( !$name ) throw new \LogicException('Invalid name specified to '.__METHOD__);
        if( !isset(self::$_navs[$name]) ) throw new \LogicException('SmartNav '.$name.' does not exist');
        return self::$_navs[$name];
    }

    public static function &get_first_nav()
    {
        if( !count(self::$_navs) ) throw new \LogicException('There are no CGSmartNav navigations defined');
        $keys = array_keys(self::$_navs);
        $key = $keys[0];
        return self::$_navs[$key];
    }
}