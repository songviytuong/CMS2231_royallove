<h3>What Does This Do?</h3>
<p>This module makes creating app navigations easier, and from within the familiar smarty environment. By using smarty tags to 'build' navigations where you specify the URL's and other attributes.  Then calling the module to render the navigation.</p>
<p>App developers can create navigations directly to module actions, to external sites, or to content pages with flexibility.</p>

<h3>Smarty Plugins:</h3>
<ul>
    <li><code>{nav_create nav=string [maxdepth=int]</code>
        <p>This creates the navigation object in memory.  A navigation must be created before you can add items to it.</p>
	<p>Parameters:</p>
	<ul>
	   <li><code>nav=string</code> <strong>required</strong> - A name for the navigation.</li>
	   <li><code>maxdepth=int</code> <em>(optional)</em> - An optional maximum depth of the navigation to use when rendering.</li>
	</ul>
    </li>

    <li><code>{nav_add_item nav=string name=string [options...]}</code>
        <p>Add a node to the specified navigation.</p>
	<p>Parameters:</p>
	<ul>
	   <li><code>nav=string</code> <strong>required</strong> - The name of a navigation. The navigation must already have been created using <code>{nav_create}</code></p></li>
	   <li><code>name=string</code> <strong>required</string> - A unique name for this item.</li>
	   <li><code>type=item|separator|sectionheader</code> <em>(optional)</em> - The item type.  If not specified then a regular nav item is implied.
	       <p><strong>Note:</strong> &quot;item&quot; nodes, require URL's and text... SectionHeaders require specifying a text attribute, while separators do not.</p>
	   </li>
	   <li><code>parent=string</code> <em>(optional)</em> - Optionally specify the name of a node that has already been added to the navigation that this item should be a child of.</li>
	   <li><code>url=string</code> <em>(optional)</em> - Required for regular items.  The URL that should be used in the node.</li>
	   <li><code>title=string</code> <em>(optional)</em> - A title attribute for the node.  Your template used to render the navigation may not use this.</li>
	   <li><code>classs=string</code> <em>(optional)</em> - Additional classes for this node.  Your template used to render the navigation may not use this.</li>
	   <li><code>target=string</code> <em>(optional)</em> - A target attribute for the node.  Your template used to render the navigation may not use this.</li>
	   <li><code>id=string</code> <em>(optional)</em> - An id attribute for the node.  Your template used to render the navigation may not use this.</li>
	   <li><code>key=string</code> <em>(optional)</em> - A key attribute for the node to use with the accesskey attribute.  Your template used to render the navigation may not use this, or may use it for another purpose.</li>
	</ul>
	<p><strong>Note:</strong> When creating 'item' nodes you must provide a URL and some text.</p>
    </li>

    <li><code>{nav_add_content nav=string [options...]}</code>
        <p>Automatically create nodes from the CMSMS content structure.</p>
	<p>this plugin will walk the content tree, creating nodes of eligible content objects starting at the specified start location, or the children of the specified item.  Only content items that the current user is entitled to see, are active, and are marked as &quot;Show In Menu&quot; will be added.</p>
	<p>Parameters:</p>
	<ul>
	   <li><code>nav=string</code> <strong>required</strong> - The name of a navigation. The navigation must already have been created using <code>{nav_create}</code></p></li>
	   <li><code>start=string</code> <em>(optional)</em> - 	The page alias of the start element where to build a navigation.  The selected alias AND it's eligible children will be included.  One of &quot;childrenof&quot; or &quot;start&quot; must be specified.</li>
	   <li><code>childrenof=string</code> <em>(optional)</em> - a page alias whose eligible children will be inserted into the navigation.  One of &quot;children&quot; or &quot;start&quot; must be specified.</li>
	   <li><code>maxdepth=int</code> <em>(optional)</em> - The maximum depth of nodes to include in the navigation.</li>
	   <li><code>showall=bool</code> <em>(optional)</em> - Indicates whether the 'show-in-menu' attribute of content items should be ignored when processing content items.</li>
	</ul>
    </li>

    <li><code>{nav_remove_item nav=string item=string|url=string}</code>
        <p>Remove an item, and any children from a built navigation.</p>
	<p>Parameters:</p>
	<ul>
	   <li><code>nav=string</code> <strong>required</strong> - The name of a navigation. The navigation must already have been created using <code>{nav_create}</code></p></li>
	   <li><code>item=string</code> <em>(optional)</em> - The name of the node to remove.  One of the item or url parameters must be provived.</li>
	   <li><code>url=string</code> <em>(optional)</em> - The url of the node to remove.  One of the item or url parameters must be provided.
	      <p>If specifying the URL and many nodes in your navigation contain the same URL then only the first node that matches the URL (and its children) will be removed.</p>
	   </li>
	</ul>
    </li>

    <li><code>{nav_set_active nav=string item=string}</code>
        <p>Set the name navigation item active.</p>
	<p>This method is useful in module action templates to indicate that the particular module actin that is being called should be marked as active in a navigation.</p>
	<ul>
	   <li><code>nav=string</code> <strong>required</strong> - The name of a navigation. The navigation must already have been created using <code>{nav_create}</code></p></li>
	   <li><code>item=string</code> <strong>require</strong> - The name of the item to mark as active.</li>
	</ul>
	<p>Templates that create navigations can use the &quotactive&quot; member of the node to alter the display the node differently.</p>
    </li>

    <li><code>{nav_render nav=string [template=string]}</code>
        <p>Render a navigation into HTML.  This method is a direct alternative to calling {CGSmartNav}</p>
	<p>This plugin renders a pre-built navigation into HTML.  It utilizes a smarty template, and is compatible with design manager.</p>
	<ul>
	   <li><code>nav=string</code> <strong>required</strong> - The name of a navigation. The navigation must already have been created using <code>{nav_create}</code></p></li>
	   <li><code>template=string</code> <em>(optional)</em> - The name of the template.
	       <p>If not specified, then the built in CGSmartNav template 'simple.tpl' will be used.</p>
	       <p>Design manager templates can be used by specifying the name of the template.</p>
	       <p>Alternate module, file based templates by specifying a .tpl extension at the end of the string.  i.e:  template=something.tpl</p>
	       <p>Other templates may be used by specifying the full resource specification.  i.e.: template=file:foo.tpl</p>
	   </li>
	</ul>
	<p>Templates that create navigations can use the &quotactive&quot; member of the node to alter the display the node differently.</p>
    </li>
</ul>

<h3>Rendering</h3>
<p>The rendering action of CGSmartNav receives utilizes a nested set of SmartNavItem objects and iterates through them recursively to do rendering.  This is similar to how the Navigator module operats.</p>
<p>To see the sample template developers can view the modules/CGSmartNav/templates/simple.tpl file.</p>

<h3>SmartNavItem</h3>
<p>Constants:</p>
<ul>
   <li>ITEM - for a regular item node.</li>
   <li>SEPARATOR - for a separator node.</li>
   <li>SECTIONHEADER - for a section header node.</li>
</ul>
<p>Members:</p>
<ul>
   <li>type - string</li>
   <li>text - string</li>
   <li>title - string</li>
   <li>class - string</li>
   <li>key - string</li>
   <li>id - string</li>
   <li>name - string</li>
   <li>children - a reference to any child nodes.</li>
   <li>active - boolean</li>
   <li>active_parent - boolean</li>
   <li>target - string</li>
</ul>

<h3>Simple Example:</h3>
<p>This is a simple example of creating a navigation for a simple application that allows authorized users to submit news articles.</p>
{nav_create nav=test}
{nav_add_item nav=test name="home" url="{cms_selflink href=$content_id}" text="Home"}
{nav_add_content nav=test name=about start=about}
{nav_add_item nav=test name=contact url="{cms_action_url module=CGBetterForms form=contact returnid=$content_id}" text="Contact"}
{nav_add_item nav=test name=sep0 type=separator}
{if feu_smarty::get_current_userid() < 1}
    {nav_add_item nav=test name=login url="{cms_action_url module=FrontEndUsers action=login returnid=$content_id}" text="Login"}
    {nav_add_item nav=test name=register url="{cms_action_url module=SelfRegistration returnid=$content_id}" text="Register"}
{else}
    {nav_add_item nav=test name=news_summary url="{cms_action_url module=News action=fesubmit returnid=$content_id}" text="Add News"}" text="News"}
    {nav_add_item nav=test name=settings url="{cms_action_url module=FrontEndUsers action=changesettings returnid=$content_id}" text="Change Settings"}
    {nav_add_item nav=test name=logout url="{cms_action_url module=FrontEndUsers action=logout returnid=$content_id}" text="Logout"}
{/if}
{nav_render nav=test}

<h3>Tips</h3>
<p>By default CMSMS <em>(version 2.2 and later)</em> Will pre-process and cache the output of the primary module action.  This is a good time to set a nav item as active from within your module action template.  i.e:  If the 'News fesubmit' action is in your smart navigation with a name of 'news-fesubmit'.  Then adding <code>{nav_set_active nav=mynav item='news-fesubmit'}</code> in your News fesubmit template would ensure that this item is marked as 'active' in your navigation.</p>

<h3>Support</h3>
<p>The module author is in no way obligated to provide support for this code in any fashion.  However, there are a number of resources available to help you with it:</p>
<ul>
<li>A bug tracking and feature request system has been created for this module <a href="http://dev.cmsmadesimple.org/projects/cgsimplesmarty">here</a>.  Please be verbose and descriptive when submitting bug reports and feature requests, and for bug reports ensure that you have provided sufficient information to reliably reproduce the issue.</li>
<li>Additional discussion of this module may also be found in the <a href="http://forum.cmsmadesimple.org">CMS Made Simple Forums</a>.  When describing an issue please make an effort to privide all relavant information, a thorough description of your issue, and steps to reproduce it or your discussion may be ignored.</li>
<li>The author, calguy1000, can often be found in the <a href="irc://irc.freenode.net/#cms">CMS IRC Channel</a>.</li>
<li>Lastly, you may have some success emailing the author directly.  However, please use this as a last resort, and ensure that you have followed all applicable instructions on the forge, in the forums, etc.</li>
</ul>

<h3>Copyright and License</h3>
<p>Copyright &copy; 2008, Robert Campbel <a href="mailto:calguy1000@cmsmadesimple.org">&lt;calguy1000@cmsmadesimple.org&gt;</a>. All Rights Are Reserved.</p>
<p>This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.</p>
<p>However, as a special exception to the GPL, this software is distributed
as an addon module to CMS Made Simple.  You may not use this software
in any Non GPL version of CMS Made simple, or in any version of CMS
Made simple that does not indicate clearly and obviously in its admin
section that the site was built with CMS Made simple.</p>
<p>This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
Or read it <a href="http://www.gnu.org/licenses/licenses.html#GPL">online</a></p>
