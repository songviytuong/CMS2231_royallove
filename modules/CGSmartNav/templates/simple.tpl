{strip}
{function name='do_smartnav' depth=0}
<ul class="{if $depth == 0}nav{/if} smartnav smartnav-depth{$depth}">
  {foreach $nav as $elem}
    {if $elem->type == $elem::SEPARATOR}
      <li class="nav-divider"></li>
    {elseif $elem->type == $elem::SECTIONHEADER}
      {$liclass='nav-header'}
      {if $elem->has_children()}{$liclass="parent {$liclass}"}{/if}
      {if $elem->active_parent}{$liclass="active-parent {$liclass}"}{/if}
      <li class="{$liclass}">{$elem->text}
        {if $elem->has_children()}
	  {do_smartnav nav=$elem->get_children() depth=$depth+1}
	{/if}
      </li>
    {else}
      {$liclass="item {$elem->class}"}
      {$aclass=""}
      {if $elem->active}
        {$liclass="active {$liclass}"}
        {$aclass="active {$aclass}"}
      {elseif $elem->active_parent}
        {$liclass="menuparent {$liclass}"}
      {/if}
      <li class="{$liclass}">
        <a href="{$elem->url}" id="{$elem->id}" class="{$aclass}" title="{$elem->title}" {if $elem->target}target="{$elem->target}"{/if}>{$elem->text}</a>
        {if $elem->has_children()}
          {* recursion *}
	  {do_smartnav nav=$elem->get_children() depth=$depth+1}
	{/if}
      </li>
    {/if}
  {/foreach}
</ul>
{/function}

{if count($nav) > 0}{do_smartnav}{/if}
{/strip}
