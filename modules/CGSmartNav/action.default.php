<?php
if( !isset($gCms) ) exit;

try {
    $template = \cge_param::get_string($params,'template');
    if( !$template ) $template = 'simple.tpl';

    $name = \cge_param::get_string($params,'nav');
    $nav = null;
    if( $name ) $nav = \CGSmartNav\SmartNav::get($name);
    if( !$nav ) $nav = \CGSmartNav\SmartNav::get_first_nav();

    // now find the active item.
    // if we have a name, we can look for a preset active item.
    $active = $node = null;
    if( $name ) $active = \cge_tmpdata::get('CGSmartNav::active::'.$name);
    if( $active ) {
        $node = $nav->find_by_tag('name',$active);
    }
    else {
        $this_url = urldecode(cge_url::current_url());
        if( !startswith($this_url,'http') && !startswith($this_url,'//') ) $this_url = '//'.$this_url;
        if( endswith( $this_url, '/') && $config['page_extension'] != '/' ) $this_url = substr($this_url,0,-1);
        $node = $nav->find_by_tag('url',$this_url);
    }
    if( $node ) {
        $node->active = true;
        $node = $node->get_parent();
        while( $node ) {
            $node->active_parent = true;
            $node = $node->get_parent();
        }
    }

    $tpl = $this->CreateSmartyTemplate($template);
    $tpl->assign('nav',$nav->get_children());
    $tpl->display();
}
catch( \Exception $e ) {
    audit('',$this->GetName(),' Problem displaying smartnav');
}
