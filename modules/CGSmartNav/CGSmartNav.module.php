<?php
#BEGIN_LICENSE
#-------------------------------------------------------------------------
# Module: CGSmartNav (c) 2008-2016 by Robert Campbell
#         (calguy1000@cmsmadesimple.org)
#  An addon module for CMS Made Simple to allow management of frontend
#  users, and their login process within a CMS Made Simple powered
#  website.
#
#-------------------------------------------------------------------------
# CMS - CMS Made Simple is (c) 2005 by Ted Kulp (wishy@cmsmadesimple.org)
# Visit the CMSMS Homepage at: http://www.cmsmadesimple.org
#
#-------------------------------------------------------------------------
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# However, as a special exception to the GPL, this software is distributed
# as an addon module to CMS Made Simple.  You may not use this software
# in any Non GPL version of CMS Made simple, or in any version of CMS
# Made simple that does not indicate clearly and obviously in its admin
# section that the site was built with CMS Made simple.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
# Or read it online: http://www.gnu.org/licenses/licenses.html#GPL
#
#-------------------------------------------------------------------------
#END_LICENSE
if( !isset($gCms) ) exit;

class CGSmartNav extends CGExtensions
{
    function GetFriendlyName() { return $this->Lang('friendlyname'); }
    function GetVersion() { return '1.1'; }
    function MinimumCMSVersion() { return '2.2.5'; }
    function GetAuthor() { return 'Calguy1000'; }
    function GetAuthorEmail() { return 'calguy1000@cmsmadesimple.org'; }
    function IsPluginModule() { return TRUE; }
    function HasAdmin() { return FALSE; }
    function GetAdminSection() { return 'extensions'; }
    function GetDependencies() { return [ 'CGExtensions'=>'1.59' ]; }
    function GetAdminDescription() { return $this->Lang('moddescription'); }
    function VisibleToAdminUser() { return FALSE; }
    function InstallPostMessage() { return $this->Lang('postinstall'); }
    function LazyLoadFrontend() { return FALSE; }
    function LazyLoadAdmin() { return TRUE; }
    function UninstallPostMessage() { return $this->Lang('postuninstall'); }

    public function InitializeFrontend()
    {
        $this->RegisterModulePlugin();
        $this->RestrictUnknownParams();

        \CGSmartNav\smarty_plugins::init();
        $this->SetParameterType('nav',CLEAN_STRING);
        $this->SetParameterType('template',CLEAN_STRING);
    }
} // class CGSmartNav
