<?php
namespace CGBetterForms;

class DOMLabelField extends DOMField
{
    public function getNodeValue(\DOMNode $node)
    {
        // concatenate and strip tags on all text nodes.
        $text = null;
        foreach( $node->childNodes as $child ) {
            if( $child->nodeType == 3 ) {
                $text .= $child->textContent;
            }
        }
        return trim($text);
    }
} // class
