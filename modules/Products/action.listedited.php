<?php

if (!isset($gCms)) exit;

$date1 = \cge_param::get_string($params,'date');

$date = new DateTime();
if($date1){
    $now = $date1;
}else{
    $now = $date->format('Y-m-d');
}
$compid = \cge_param::get_int($params,'compid');
$entryarray = array();
$query = "SELECT * FROM cms_module_products WHERE owner = $compid AND modified_date LIKE '$now%' ORDER BY modified_date DESC";
$dbresult = $db->Execute($query);

$rowclass = 'row1';
$theme = cms_utils::get_theme_object();
while ($dbresult && $row = $dbresult->FetchRow()) {
    $onerow = new stdClass();
    $onerow->id = $row["id"];
    $onerow->product_name = $row["product_name"];
    $onerow->product_name_vi = $row["product_name_vi"];
    $onerow->sku = $row["sku"];
    $onerow->modified_date = $row["modified_date"];
    $onerow->edit_url = $this->CreateURL($id,'editproduct',$returnid, array('compid'=>$row['id']));
    $entryarray[] = $onerow;

    ($rowclass=="row1"?$rowclass="row2":$rowclass="row1");
  }

  
$this->smarty->assign_by_ref('items', $entryarray);
$this->smarty->assign('itemcount', count($entryarray));
$smarty->assign('startform', $this->CreateFormStart($id, 'editproduct', $returnid, 'post', 'multipart/form-data'));
$smarty->assign('endform', $this->CreateFormEnd());
#Display template
echo $this->ProcessTemplate('listedited.tpl');
?>