<?php

function smarty_function_title($params, &$smarty)
{
	$contentobj = CmsApp::get_instance()->get_content_object();
	$result = '';

	if (!is_object($contentobj)) {
		// We've a custom error message...  set a current timestamp
		$result = "404 Error";
	} else {
		$result = cms_htmlentities($contentobj->Name());
		$result = preg_replace("/\{\/?php\}/", "", $result);
	}

	if (isset($params['assign'])) {
		$smarty->assign(trim($params['assign']), $result);
		return;
	}
	return $result;
}

function smarty_cms_about_function_title()
{
	?>
	<p>Author: Ted Kulp&lt;tedkulp@users.sf.net&gt;</p>

	<p>Change History:</p>
	<ul>
		<li>None</li>
	</ul>
<?php
}
?>