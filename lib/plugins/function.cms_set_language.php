<?php

function smarty_function_cms_set_language($params, &$smarty)
{
  $lang = null;
  if (isset($params['lang'])) {
    $lang = trim($params['lang']);
  }
  $res = CmsNlsOperations::set_language($lang);
}
