<?php

function smarty_function_modified_date($params, &$smarty)
{
	$content_obj = CmsApp::get_instance()->get_content_object();

	$format = "%x %X";
	if (!empty($params['format'])) $format = $params['format'];
	if (is_object($content_obj) && $content_obj->GetModifiedDate() > -1) {
		$time = $content_obj->GetModifiedDate();
		$str = cms_htmlentities(strftime($format, $time));

		if (isset($params['assign'])) {
			$smarty->assign($params['assign'], $str);
			return;
		}
		return $str;
	}
}

function smarty_cms_about_function_modified_date()
{
	?>
	<p>Author: Ted Kulp&lt;tedkulp@users.sf.net&gt;</p>

	<p>Change History:</p>
	<ul>
		<li>Added assign paramater (calguy1000)</li>
	</ul>
<?php
}
?>