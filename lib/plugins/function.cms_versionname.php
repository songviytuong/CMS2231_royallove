<?php

function smarty_function_cms_versionname($params, $smarty)
{
	global $CMS_VERSION_NAME;
	if (isset($params['assign'])) {
		$smarty->assign(trim($params['assign']), $CMS_VERSION_NAME);
		return;
	}
	return $CMS_VERSION_NAME;
}

function smarty_cms_about_function_cms_versionname()
{
	?>
	<p>Author: Ted Kulp&lt;tedkulp@users.sf.net&gt;</p>
	<p>Version: 1.0</p>
	<p>
		Change History:<br />
		None
	</p>
<?php
}
?>