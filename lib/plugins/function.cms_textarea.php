<?php

function smarty_function_cms_textarea($params, &$smarty)
{
  if (!isset($params['name'])) throw new CmsInvalidDataException('syntax_area misssing parameter: name');
  if (isset($params['prefix'])) $params['name'] = $params['prefix'] . $params['name'];

  $out = CmsFormUtils::create_textarea($params);
  if (isset($params['assign'])) {
    $smarty->assign(trim($params['assign']), $out);
    return;
  }
  return $out;
}